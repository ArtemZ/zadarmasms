package com.zadarma;

/**
 * Created with IntelliJ IDEA.
 * User: artemz
 * Date: 5/12/13
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ZadarmaSetup {
    String getUsername();
    String getPassword();
    String getAuthUrl();
    String getSendSmsUrl();
}
