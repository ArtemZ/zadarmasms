package com.zadarma;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: artemz
 * Date: 5/12/13
 * Time: 4:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class Datasender {
    public static void sendData(AuthenticationToken auth, ZadarmaSetup setup, String data) throws Exception {
        URL url = new URL(setup.getSendSmsUrl());
/*        URL url = new URL("https://ss.zadarma.com/connect/sms/");*/
        URLConnection conn = url.openConnection();
        conn.setReadTimeout(30 * 1000); //read timeout, in milliseconds
        conn.setConnectTimeout(30 * 1000); //connection timeout, in milliseconds
        //setting cookies
        conn.setRequestProperty("Cookie", auth.getToken(setup));
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.flush();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        StringBuffer buff = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            buff.append(line);
        }
        System.out.println(buff.toString());
        rd.close();
        wr.close();
        //return buff.toString();
    }
    public static String getCookie(String authUrl, String data, String cookieName) throws Exception{
        URL url = new URL(authUrl);
        URLConnection conn = url.openConnection();
        Pattern pattern = Pattern.compile("^"+ cookieName +"=[^;]+");
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.flush();

        /*BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        StringBuffer buff = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            buff.append(line);
        }
        System.out.println(buff.toString());
        rd.close();*/




        wr.close();
        Matcher m = null;
        //Getting cookies
        String headerName=null;
        for (int i=1; (headerName = conn.getHeaderFieldKey(i))!=null; i++) {
            if (headerName.equals("Set-Cookie") && conn.getHeaderField(i).toString().startsWith(cookieName)) {
                m = pattern.matcher(conn.getHeaderField(i).toString());
            }
        }
        if(m.find()){
            //System.out.println(m.group(0));
            return m.group(0);
        } else {
            throw new Exception("Authentification failure");
        }
    }
}
