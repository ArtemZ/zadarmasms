package com.zadarma;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: artemz
 * Date: 5/11/13
 * Time: 6:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class Zadarma {
    public static void main(String[] args) throws Exception {

    }
    public Zadarma(){}
    public void sendSms(ZadarmaSetup setup, Sms sms) throws Exception {
        //prepare the data
        String data = URLEncoder.encode("message", "UTF-8") + "=" + URLEncoder.encode(sms.getText(), "UTF-8") + "&phonenumber=";
        for(int i=0; i < sms.getRecipients().size(); i++){
            if(i > 0){
                data += URLEncoder.encode(",", "UTF-8") + sms.getRecipients().get(i);
            } else {
                data += sms.getRecipients().get(i);
            }
        }
        Datasender.sendData(new AuthenticationToken() {
            @Override
            public String getToken(ZadarmaSetup setup) throws Exception{
                String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(setup.getUsername(), "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(setup.getPassword(), "UTF-8");
                return Datasender.getCookie(setup.getAuthUrl(), data, "PHPSESSID");
            }
        }, setup, data);

    }
}
