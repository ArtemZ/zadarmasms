package com.zadarma;

/**
 * Created with IntelliJ IDEA.
 * User: artemz
 * Date: 5/12/13
 * Time: 4:52 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AuthenticationToken {
    String getToken(ZadarmaSetup setup) throws Exception;
}
