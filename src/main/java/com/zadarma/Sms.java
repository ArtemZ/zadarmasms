package com.zadarma;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: artemz
 * Date: 5/12/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Sms {
    String getText();
    List<String> getRecipients();
}
